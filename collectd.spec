%global __provides_exclude_from ^%{_libdir}/collectd/.*\\.so$
%undefine _strict_symbol_defs_build
%global _privatelibs lib(pq|pgtypes|ecpg_compat|ecpg)\\.so*
%global __requires_exclude %{_privatelibs}

Summary:          Statistics collection daemon for filling RRD files
Name:             collectd
Version:          5.9.0
Release:          4
License:          GPLv2
URL:              https://collectd.org/

Source:           https://collectd.org/files/%{name}-%{version}.tar.bz2
Source1:          collectd-httpd.conf
Source2:          collectd.service
Source3:          apache.conf
Source4:          email.conf
Source5:          mysql.conf
Source6:          nginx.conf
Source7:          sensors.conf
Source8:          snmp.conf
Source9:          rrdtool.conf
Source10:         onewire.conf

Patch0:           %{name}-include-collectd.d.patch

BuildRequires:    perl-devel
BuildRequires:    perl-generators
BuildRequires:    perl-interpreter
BuildRequires:    perl(ExtUtils::MakeMaker)
BuildRequires:    perl(ExtUtils::Embed)
BuildRequires:    libgcrypt-devel
Requires(post):   systemd
Requires(preun):  systemd
Requires(postun): systemd

%description
collectd is a daemon which collects system performance statistics periodically
and provides mechanisms to store the values in a variety of ways,
for example in RRD files.


%package amqp
Summary:       AMQP plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: librabbitmq-devel
%description amqp
This plugin can be used to communicate with other instances of collectd
or third party applications using an AMQP message broker.


%package apache
Summary:       Apache plugin for collectd
Requires:      %{name} = %{version}-%{release}
%description apache
This plugin collects data provided by Apache's 'mod_status'.


%package ascent
Summary:       Ascent plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: curl-devel
BuildRequires: libxml2-devel
%description ascent
This plugin collects data about an Ascent server,
a free server for the "World of Warcraft" game.


%package bind
Summary:       Bind plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: curl-devel
BuildRequires: libxml2-devel
%description bind
This plugin retrieves statistics from the BIND dns server.


%package ceph
Summary:       Ceph plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: yajl-devel
%description ceph
This plugin collects data from Ceph.


%package chrony
Summary:       Chrony plugin for collectd
Requires:      %{name} = %{version}-%{release}
%description chrony
Chrony plugin for collectd


%package -n collectd-utils
Summary:       Collectd utilities
Requires:      libcollectdclient = %{version}-%{release}
Requires:      %{name} = %{version}-%{release}
%description -n collectd-utils
Collectd utilities


%package curl
Summary:       Curl plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: curl-devel
%description curl
This plugin reads webpages with curl


%package curl_json
Summary:       Curl JSON plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: curl-devel
BuildRequires: yajl-devel
%description curl_json
This plugin retrieves JSON data via curl.


%package curl_xml
Summary:       Curl XML plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: curl-devel
BuildRequires: libxml2-devel
%description curl_xml
This plugin retrieves XML data via curl.


%package dbi
Summary:       DBI plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: libdbi-devel
%description dbi
This plugin uses the dbi library to connect to various databases,
execute SQL statements and read back the results.


%package disk
Summary:       Disk plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: systemd-devel
%description disk
This plugin collects statistics of harddisk and, where supported, partitions.


%package dns
Summary:       DNS traffic analysis plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: libpcap-devel
%description dns
This plugin collects DNS traffic data.


%package drbd
Summary:       DRBD plugin for collectd
Requires:      %{name} = %{version}-%{release}
%description drbd
This plugin collects data from DRBD.


%package email
Summary:       Email plugin for collectd
Requires:      %{name} = %{version}-%{release}
%description email
This plugin collects data provided by spamassassin.


%package generic-jmx
Summary:       Generic JMX plugin for collectd
Requires:      %{name} = %{version}-%{release}
%description generic-jmx
This plugin collects data provided by JMX.


%package hugepages
Summary:       Hugepages plugin for collectd
Requires:      %{name} = %{version}-%{release}
%description hugepages
This plugin collects statistics about hugepage usage.


%package ipmi
Summary:       IPMI plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: OpenIPMI-devel
%description ipmi
This plugin for collectd provides IPMI support.


%package iptables
Summary:       Iptables plugin for collectd
Requires:      collectd = %{version}-%{release}
BuildRequires: iptables-devel
%description iptables
This plugin collects data from iptables counters.


%package ipvs
Summary:       IPVS plugin for collectd
Requires:      %{name} = %{version}-%{release}
%description ipvs
This plugin collects data from IPVS.


%package java
Summary:       Java bindings for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: java-devel
BuildRequires: jpackage-utils
%description java
These are the Java bindings for collectd.


%package -n libcollectdclient
Summary:       Collectd client library
%description -n libcollectdclient
Collectd client library.


%package -n libcollectdclient-devel
Summary:       Development files for libcollectdclient
Requires:      libcollectdclient = %{version}-%{release}
%description -n libcollectdclient-devel
Development files for libcollectdclient.


%package log_logstash
Summary:       Logstash plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: yajl-devel
%description log_logstash
This plugin formats messages as JSON events for Logstash


%package lua
Summary:       Lua plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: lua-devel
%description lua
The Lua plugin embeds a Lua interpreter into collectd and exposes the
application programming interface (API) to Lua scripts.


%package mcelog
Summary:       Mcelog plugin for collectd
Requires:      %{name} = %{version}-%{release}
%description mcelog
This plugin monitors machine check exceptions reported by mcelog and generates
appropriate notifications when machine check exceptions are detected.


%package memcachec
Summary:       Memcachec plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: libmemcached-devel
%description memcachec
This plugin connects to a memcached server, queries one or more
given pages and parses the returned data according to user specification.


%package mysql
Summary:       MySQL plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: mariadb-connector-c-devel
%description mysql
MySQL querying plugin. This plugin provides data of issued commands,
called handlers and database traffic.


%package netlink
Summary:       Netlink plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: iproute-static, libmnl-devel
%description netlink
This plugin uses a netlink socket to query the Linux kernel
about statistics of various interface and routing aspects.


%package nginx
Summary:       Nginx plugin for collectd
Requires:      %{name} = %{version}-%{release}
%description nginx
This plugin collects data provided by Nginx.


%package notify_desktop
Summary:       Notify desktop plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: libnotify-devel
%description notify_desktop
This plugin sends a desktop notification to a notification daemon,
as defined in the Desktop Notification Specification.


%package openldap
Summary:       OpenLDAP plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: openldap-devel
%description openldap
This plugin for collectd reads monitoring information
from OpenLDAP's cn=Monitor subtree.


%package ovs_events
Summary:       Open vSwitch events plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: yajl-devel
%description ovs_events
This plugin monitors the link status of Open vSwitch (OVS) connected
interfaces, dispatches the values to collectd and sends notifications
whenever a link state change occurs in the OVS database.


%package ovs_stats
Summary:       Open vSwitch stats plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: yajl-devel
%description ovs_stats
This plugin collects statictics of OVS connected bridges and interfaces.

%package -n perl-Collectd
Summary:       Perl bindings for collectd
Requires:      %{name} = %{version}-%{release}
Requires:      perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
%description -n perl-Collectd
This package contains the Perl bindings and plugin for collectd.


%package pinba
Summary:       Pinba plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: protobuf-c-devel
%description pinba
This plugin receives profiling information from Pinba,
an extension for the PHP interpreter.


%package postgresql
Summary:       PostgreSQL plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: postgresql-server-devel
BuildRequires: postgresql-private-devel
%description postgresql
PostgreSQL querying plugin. This plugins provides data of issued commands,
called handlers and database traffic.


%package python
Summary:       Python plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: python3-devel
%description python
The Python plugin embeds a Python interpreter into Collectd and exposes the
application programming interface (API) to Python-scripts.


%package rrdcached
Summary:       RRDCacheD plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: rrdtool-devel
%description rrdcached
This plugin uses the RRDtool accelerator daemon, rrdcached(1),
to store values to RRD files in an efficient manner.


%package rrdtool
Summary:       RRDTool plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: rrdtool-devel
%description rrdtool
This plugin for collectd provides rrdtool support.


%ifnarch ppc sparc sparc64
%package sensors
Summary:       Libsensors module for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: lm_sensors-devel
%description sensors
This plugin for collectd provides querying of sensors supported by
lm_sensors.
%endif


%package smart
Summary:       SMART plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: libatasmart-devel
%description smart
This plugin for collectd collects SMART statistics,
notably load cycle count, temperature and bad sectors.


%package snmp
Summary:       SNMP module for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: net-snmp-devel
%description snmp
This plugin for collectd provides querying of net-snmp.


%package snmp_agent
Summary:       SNMP AgentX plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: net-snmp-devel
%description snmp_agent
This plugin is an AgentX subagent that receives and handles queries
from a SNMP master agent and returns the data collected by read plugins.


%package synproxy
Summary:       Synproxy plugin for collectd
Requires:      %{name} = %{version}-%{release}
%description synproxy
This plugin provides statistics for Linux SYNPROXY available since 3.12


%package varnish
Summary:       Varnish plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: varnish-libs-devel
%description varnish
This plugin collects information about Varnish, an HTTP accelerator.


%ifnarch ppc sparc sparc64
%package virt
Summary:       Libvirt plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: libvirt-devel
BuildRequires: libxml2-devel
%description virt
This plugin collects information from virtualized guests.
%endif


%package web
Summary:       Contrib web interface to viewing rrd files
Requires:      %{name} = %{version}-%{release}
Requires:      collectd-rrdtool = %{version}-%{release}
Requires:      perl-HTML-Parser, perl-Regexp-Common, rrdtool-perl, httpd
%description web
This package will allow for a simple web interface to view rrd files created by
collectd.


%package write_http
Summary:       HTTP output plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: curl-devel
%description write_http
This plugin can send data to Redis.


%package write_kafka
Summary:       Kafka output plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: librdkafka-devel
%description write_kafka
This sends values to Kafka, a distributed messaging system.


%package write_mongodb
Summary:       MongoDB output plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: mongo-c-driver-devel
%description write_mongodb
This plugin sends values to MongoDB.


%package write_prometheus
Summary:       Prometheus output plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: libmicrohttpd-devel
%description write_prometheus
This plugin exposes collected values using an embedded HTTP
server, turning the collectd daemon into a Prometheus exporter.


%package write_riemann
Summary:       Riemann output plugin for collectd
Requires:      %{name} = %{version}-%{release}
BuildRequires: riemann-c-client-devel
%description write_riemann
This plugin can send data to Riemann.


%package write_sensu
Summary:       Sensu output plugin for collectd
Requires:      %{name} = %{version}-%{release}
%description write_sensu
This plugin can send data to Sensu.


%package write_syslog
Summary:       syslog output plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name} = %{version}-%{release}

Provides: %{name}-write-syslog = %{version}-%{release}

%description write_syslog
This plugin can send data to syslog.


%package write_tsdb
Summary:       OpenTSDB output plugin for collectd
Requires:      %{name} = %{version}-%{release}
%description write_tsdb
This plugin can send data to OpenTSDB.


%package zookeeper
Summary:       Zookeeper plugin for collectd
Requires:      %{name} = %{version}-%{release}
%description zookeeper
This is a collectd plugin that reads data from Zookeeper's MNTR command.


%prep
%autosetup -v -p1

# recompile generated files
touch src/pinba.proto
sed -i 's#longintrepr\.h#cpython/&#' src/cpython.h


%build
%configure \
    --disable-dependency-tracking \
    --enable-all-plugins \
    --disable-static \
    --disable-amqp1 \
    --disable-apple_sensors \
    --disable-aquaero \
    --disable-barometer \
    --disable-dpdkevents \
    --disable-dpdkstat \
    --disable-gmond \
    --disable-gps \
    --disable-gpu_nvidia \
    --disable-grpc \
    --disable-intel_pmu \
    --disable-intel_rdt \
    --disable-lpar \
    --disable-lvm \
    --disable-mic \
    --disable-modbus \
    --disable-mqtt \
    --disable-netapp \
    --disable-notify_email \
    --disable-nut \
    --disable-onewire \
    --disable-oracle \
%ifarch s390 s390x
    --disable-pcie_errors \
%endif
    --disable-pf \
    --disable-ping \
    --disable-redis \
    --disable-routeros \
%ifarch ppc sparc sparc64
    --disable-sensors \
%endif
    --disable-sigrok \
    --disable-tape \
    --disable-tokyotyrant \
    --disable-turbostat \
    --disable-write_redis \
    --disable-xencpu \
    --disable-xmms \
    --disable-zone \
    --with-java=%{java_home}/ \
    --with-python=%{_bindir}/python3 \
    --with-perl-bindings=INSTALLDIRS=vendor \
    --disable-werror \
    AR_FLAGS="-cr"

make %{?_smp_mflags}


%install
rm -rf contrib/SpamAssassin
make install DESTDIR="%{buildroot}"

install -Dp -m0644 src/collectd.conf %{buildroot}%{_sysconfdir}/collectd.conf
install -Dp -m0644 %{SOURCE2} %{buildroot}%{_unitdir}/collectd.service
install -d -m0755 %{buildroot}%{_localstatedir}/lib/collectd/rrd
install -d -m0755 %{buildroot}%{_datadir}/collectd/collection3/
install -d -m0755 %{buildroot}%{_sysconfdir}/httpd/conf.d/

find contrib/ -type f -exec chmod a-x {} \;

# Remove Perl hidden .packlist files.
find %{buildroot} -name .packlist -delete
# Remove Perl temporary file perllocal.pod
find %{buildroot} -name perllocal.pod -delete

# copy web interface
cp -ad contrib/collection3/* %{buildroot}%{_datadir}/collectd/collection3/
cp -pv %{buildroot}%{_datadir}/collectd/collection3/etc/collection.conf %{buildroot}%{_sysconfdir}/collection.conf
ln -rsf %{_sysconfdir}/collection.conf %{buildroot}%{_datadir}/collectd/collection3/etc/collection.conf
cp -pv %{SOURCE1} %{buildroot}%{_sysconfdir}/httpd/conf.d/collectd.conf
chmod +x %{buildroot}%{_datadir}/collectd/collection3/bin/*.cgi

# Move the Perl examples to a separate directory.
mkdir perl-examples
find contrib -name '*.p[lm]' -exec mv {} perl-examples/ \;

# Move config contribs
mkdir -p %{buildroot}%{_sysconfdir}/collectd.d/
cp %{SOURCE3} %{buildroot}%{_sysconfdir}/collectd.d/apache.conf
cp %{SOURCE4} %{buildroot}%{_sysconfdir}/collectd.d/email.conf
cp %{SOURCE5} %{buildroot}%{_sysconfdir}/collectd.d/mysql.conf
cp %{SOURCE6} %{buildroot}%{_sysconfdir}/collectd.d/nginx.conf
cp %{SOURCE7} %{buildroot}%{_sysconfdir}/collectd.d/sensors.conf
cp %{SOURCE8} %{buildroot}%{_sysconfdir}/collectd.d/snmp.conf
cp %{SOURCE9} %{buildroot}%{_sysconfdir}/collectd.d/rrdtool.conf
cp %{SOURCE10} %{buildroot}%{_sysconfdir}/collectd.d/onewire.conf

# configs for subpackaged plugins
%ifnarch s390 s390x
for p in dns ipmi libvirt nut perl ping postgresql
%else
for p in dns ipmi libvirt perl ping postgresql
%endif
do
cat > %{buildroot}%{_sysconfdir}/collectd.d/$p.conf <<EOF
LoadPlugin $p
EOF
done

# *.la files shouldn't be distributed.
rm -f %{buildroot}/%{_libdir}/{collectd/,}*.la


%check
make check


%post
%systemd_post collectd.service


%preun
%systemd_preun collectd.service


%postun
%systemd_postun_with_restart collectd.service


%files
%license COPYING
%doc AUTHORS ChangeLog README
%config(noreplace) %{_sysconfdir}/collectd.conf
%config(noreplace) %{_sysconfdir}/collectd.d/
%exclude %{_sysconfdir}/collectd.d/apache.conf
%exclude %{_sysconfdir}/collectd.d/dns.conf
%exclude %{_sysconfdir}/collectd.d/email.conf
%exclude %{_sysconfdir}/collectd.d/ipmi.conf
%exclude %{_sysconfdir}/collectd.d/libvirt.conf
%exclude %{_sysconfdir}/collectd.d/mysql.conf
%exclude %{_sysconfdir}/collectd.d/nginx.conf
%ifnarch s390 s390x
%exclude %{_sysconfdir}/collectd.d/nut.conf
%endif
%exclude %{_sysconfdir}/collectd.d/onewire.conf
%exclude %{_sysconfdir}/collectd.d/perl.conf
%exclude %{_sysconfdir}/collectd.d/ping.conf
%exclude %{_sysconfdir}/collectd.d/postgresql.conf
%exclude %{_datadir}/collectd/postgresql_default.conf
%exclude %{_sysconfdir}/collectd.d/rrdtool.conf
%exclude %{_sysconfdir}/collectd.d/sensors.conf
%exclude %{_sysconfdir}/collectd.d/snmp.conf

%{_unitdir}/collectd.service
%{_sbindir}/collectd
%{_sbindir}/collectdmon
%dir %{_localstatedir}/lib/collectd/

%dir %{_libdir}/collectd

%{_libdir}/collectd/aggregation.so
%{_libdir}/collectd/apcups.so
%{_libdir}/collectd/battery.so
%{_libdir}/collectd/cgroups.so
%{_libdir}/collectd/conntrack.so
%{_libdir}/collectd/contextswitch.so
%{_libdir}/collectd/cpu.so
%{_libdir}/collectd/cpufreq.so
%{_libdir}/collectd/cpusleep.so
%{_libdir}/collectd/csv.so
%{_libdir}/collectd/df.so
%{_libdir}/collectd/entropy.so
%{_libdir}/collectd/ethstat.so
%{_libdir}/collectd/exec.so
%{_libdir}/collectd/fhcount.so
%{_libdir}/collectd/filecount.so
%{_libdir}/collectd/fscache.so
%{_libdir}/collectd/hddtemp.so
%{_libdir}/collectd/interface.so
%{_libdir}/collectd/ipc.so
%{_libdir}/collectd/irq.so
%{_libdir}/collectd/load.so
%{_libdir}/collectd/logfile.so
%{_libdir}/collectd/madwifi.so
%{_libdir}/collectd/match_empty_counter.so
%{_libdir}/collectd/match_hashed.so
%{_libdir}/collectd/match_regex.so
%{_libdir}/collectd/match_timediff.so
%{_libdir}/collectd/match_value.so
%{_libdir}/collectd/mbmon.so
%{_libdir}/collectd/md.so
%{_libdir}/collectd/memcached.so
%{_libdir}/collectd/memory.so
%{_libdir}/collectd/multimeter.so
%{_libdir}/collectd/network.so
%{_libdir}/collectd/nfs.so
%{_libdir}/collectd/notify_nagios.so
%{_libdir}/collectd/ntpd.so
%{_libdir}/collectd/numa.so
%{_libdir}/collectd/olsrd.so
%{_libdir}/collectd/openvpn.so
%{_libdir}/collectd/powerdns.so
%ifnarch s390 s390x
%{_libdir}/collectd/pcie_errors.so
%endif
%{_libdir}/collectd/processes.so
%{_libdir}/collectd/protocols.so
%{_libdir}/collectd/serial.so
%{_libdir}/collectd/statsd.so
%{_libdir}/collectd/swap.so
%{_libdir}/collectd/syslog.so
%{_libdir}/collectd/table.so
%{_libdir}/collectd/tail.so
%{_libdir}/collectd/tail_csv.so
%{_libdir}/collectd/target_notification.so
%{_libdir}/collectd/target_replace.so
%{_libdir}/collectd/target_scale.so
%{_libdir}/collectd/target_set.so
%{_libdir}/collectd/target_v5upgrade.so
%{_libdir}/collectd/tcpconns.so
%{_libdir}/collectd/teamspeak2.so
%{_libdir}/collectd/ted.so
%{_libdir}/collectd/thermal.so
%{_libdir}/collectd/threshold.so
%{_libdir}/collectd/unixsock.so
%{_libdir}/collectd/uptime.so
%{_libdir}/collectd/users.so
%{_libdir}/collectd/uuid.so
%{_libdir}/collectd/vmem.so
%{_libdir}/collectd/vserver.so
%{_libdir}/collectd/wireless.so
%{_libdir}/collectd/write_graphite.so
%{_libdir}/collectd/write_log.so
%{_libdir}/collectd/write_stackdriver.so
%{_libdir}/collectd/write_syslog.so
%{_libdir}/collectd/zfs_arc.so

%dir %{_datadir}/collectd/
%{_datadir}/collectd/types.db

%doc %{_mandir}/man1/collectd.1*
%doc %{_mandir}/man1/collectdmon.1*
%doc %{_mandir}/man5/collectd.conf.5*
%doc %{_mandir}/man5/collectd-exec.5*
%doc %{_mandir}/man5/collectd-threshold.5*
%doc %{_mandir}/man5/collectd-unixsock.5*
%doc %{_mandir}/man5/types.db.5*


%files -n libcollectdclient-devel
%dir %{_includedir}/collectd/
%{_includedir}/collectd/client.h
%{_includedir}/collectd/lcc_features.h
%{_includedir}/collectd/network.h
%{_includedir}/collectd/network_buffer.h
%{_includedir}/collectd/network_parse.h
%{_includedir}/collectd/server.h
%{_includedir}/collectd/types.h
%{_libdir}/pkgconfig/libcollectdclient.pc
%{_libdir}/libcollectdclient.so


%files -n libcollectdclient
%{_libdir}/libcollectdclient.so.1
%{_libdir}/libcollectdclient.so.1.1.0


%files -n collectd-utils
%{_bindir}/collectd-nagios
%{_bindir}/collectd-tg
%{_bindir}/collectdctl
%{_mandir}/man1/collectdctl.1*
%{_mandir}/man1/collectd-nagios.1*
%{_mandir}/man1/collectd-tg.1*

%files amqp
%{_libdir}/collectd/amqp.so


%files apache
%{_libdir}/collectd/apache.so
%config(noreplace) %{_sysconfdir}/collectd.d/apache.conf


%files ascent
%{_libdir}/collectd/ascent.so


%files bind
%{_libdir}/collectd/bind.so


%files ceph
%{_libdir}/collectd/ceph.so


%files chrony
%{_libdir}/collectd/chrony.so


%files curl
%{_libdir}/collectd/curl.so


%files curl_json
%{_libdir}/collectd/curl_json.so


%files curl_xml
%{_libdir}/collectd/curl_xml.so


%files disk
%{_libdir}/collectd/disk.so


%files dbi
%{_libdir}/collectd/dbi.so


%files dns
%{_libdir}/collectd/dns.so
%config(noreplace) %{_sysconfdir}/collectd.d/dns.conf


%files drbd
%{_libdir}/collectd/drbd.so


%files email
%{_libdir}/collectd/email.so
%config(noreplace) %{_sysconfdir}/collectd.d/email.conf
%doc %{_mandir}/man5/collectd-email.5*


%files generic-jmx
%{_datadir}/collectd/java/generic-jmx.jar


%files hugepages
%{_libdir}/collectd/hugepages.so


%files ipmi
%{_libdir}/collectd/ipmi.so
%config(noreplace) %{_sysconfdir}/collectd.d/ipmi.conf


%files iptables
%{_libdir}/collectd/iptables.so


%files ipvs
%{_libdir}/collectd/ipvs.so


%files java
%{_libdir}/collectd/java.so
%dir %{_datadir}/collectd/java/
%{_datadir}/collectd/java/collectd-api.jar
%doc %{_mandir}/man5/collectd-java.5*

%files log_logstash
%{_libdir}/collectd/log_logstash.so


%files lua
%{_mandir}/man5/collectd-lua*
%{_libdir}/collectd/lua.so


%files mcelog
%{_libdir}/collectd/mcelog.so


%files memcachec
%{_libdir}/collectd/memcachec.so


%files mysql
%{_libdir}/collectd/mysql.so
%config(noreplace) %{_sysconfdir}/collectd.d/mysql.conf


%files netlink
%{_libdir}/collectd/netlink.so


%files nginx
%{_libdir}/collectd/nginx.so
%config(noreplace) %{_sysconfdir}/collectd.d/nginx.conf


%files notify_desktop
%{_libdir}/collectd/notify_desktop.so


%files openldap
%{_libdir}/collectd/openldap.so


%files ovs_events
%{_libdir}/collectd/ovs_events.so


%files ovs_stats
%{_libdir}/collectd/ovs_stats.so

%files -n perl-Collectd
%doc perl-examples/*
%{_libdir}/collectd/perl.so
%{perl_vendorlib}/Collectd.pm
%{perl_vendorlib}/Collectd/
%config(noreplace) %{_sysconfdir}/collectd.d/perl.conf
%doc %{_mandir}/man5/collectd-perl.5*
%doc %{_mandir}/man3/Collectd::Unixsock.3pm*


%files pinba
%{_libdir}/collectd/pinba.so


%files postgresql
%{_libdir}/collectd/postgresql.so
%config(noreplace) %{_sysconfdir}/collectd.d/postgresql.conf
%{_datadir}/collectd/postgresql_default.conf


%files python
%{_libdir}/collectd/python.so
%doc %{_mandir}/man5/collectd-python.5*


%files rrdcached
%{_libdir}/collectd/rrdcached.so


%files rrdtool
%{_libdir}/collectd/rrdtool.so
%config(noreplace) %{_sysconfdir}/collectd.d/rrdtool.conf


%ifnarch ppc sparc sparc64
%files sensors
%{_libdir}/collectd/sensors.so
%config(noreplace) %{_sysconfdir}/collectd.d/sensors.conf
%endif


%files smart
%{_libdir}/collectd/smart.so


%files snmp
%{_libdir}/collectd/snmp.so
%config(noreplace) %{_sysconfdir}/collectd.d/snmp.conf
%doc %{_mandir}/man5/collectd-snmp.5*


%files snmp_agent
%{_libdir}/collectd/snmp_agent.so


%files synproxy
%{_libdir}/collectd/synproxy.so


%files varnish
%{_libdir}/collectd/varnish.so


%ifnarch ppc sparc sparc64
%files virt
%{_libdir}/collectd/virt.so
%config(noreplace) %{_sysconfdir}/collectd.d/libvirt.conf
%endif


%files web
%{_datadir}/collectd/collection3/
%config(noreplace) %{_sysconfdir}/httpd/conf.d/collectd.conf
%config(noreplace) %{_sysconfdir}/collection.conf


%files write_http
%{_libdir}/collectd/write_http.so


%files write_kafka
%{_libdir}/%{name}/write_kafka.so


%files write_mongodb
%{_libdir}/%{name}/write_mongodb.so


%files write_prometheus
%{_libdir}/collectd/write_prometheus.so


%files write_riemann
%{_libdir}/collectd/write_riemann.so


%files write_sensu
%{_libdir}/collectd/write_sensu.so


%files write_syslog
%{_libdir}/collectd/write_syslog.so


%files write_tsdb
%{_libdir}/collectd/write_tsdb.so


%files zookeeper
%{_libdir}/collectd/zookeeper.so


%changelog
* Fri Aug 18 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 5.9.0-4
- Fix missing longintrepr.h

* Fri Mar 10 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 5.9.0-3
- Add buildrequires

* Wed Apr 06 2022 liqiuyu <liqiuyu@kylinos.cn> - 5.9.0-2
- Change buildrequires from postgresql-13-server-devel to postgresql-server-devel

* Tue Aug 17 2021 Python_Bot <Python_Bot@openeuler.org> - 5.9.0-1
- Init package
